import pandas as pd

raw_data = pd.read_csv("C:/Users/TESS/Downloads/amazon_sales_cleaned_sql.csv")
print(raw_data.dtypes)

def transform(raw_data):
    # Convert the "Order Date" column to type datetime
    raw_data["order_date"] = pd.to_datetime(raw_data["order_date"], format="%m/%d/%y %H:%M")
    
    # Only keep items under ten dollars
    clean_data = raw_data.loc[raw_data["price_each"] < 10, :]
    
    return clean_data

clean_sales_data = transform(raw_data)


# Check the data types of each column
print(clean_sales_data.dtypes)

#Output
#order_id              int64
#product              object
#quantity_ordered      int64
#price_each          float64
#order_date           object
#purchase_address     object
#dtype: object
#order_id                     int64
#product                     object
#quantity_ordered             int64
#price_each                 float64
#order_date          datetime64[ns]
#purchase_address            object
#dtype: object